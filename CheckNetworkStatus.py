#!/usr/bin/python

# Note need to remove file on Raspberry Pi
# /etc/ifplugd/action.d/action_wpa (this is a symlink to
# ../../wpa_supplicant/action_wpa.sh)
# then bring up wlan0 > sudo ifup wlan0

# insure DNSUtils installed
# sudo apt-get install dnsuntils

import sys
import sqlite3
import subprocess
import datetime
import sense_flash
import uuid

try:
    # check to see if this is a Raspberry Pi with a Sense Hat
    from sense_hat import SenseHat
    raspberry = True
    sense = SenseHat()
    # dim the display by halfing the rgb value
    red = (int(255/2), 0, 0)
    green = (0,int(255/2),0)
    sense.low_light = True
    # dont clear at night.
    if datetime.datetime.now().hour > 20 or datetime.datetime.now().hour < 7:
        pass
    else:
        sense.clear()

except ImportError:
    # Nope, not a Pi. Probably Nick testing on Mac
    raspberry = False
    print "No Pi"


if raspberry:
    # if on Rasperry then hardcode the path to the DB (required for running as cron)
    conn = sqlite3.connect('/home/pi/rp-networkchecker/networklog.db')
else:
    # running interactively so just store locally
    conn = sqlite3.connect('networklog.db')

c = conn.cursor()

# different networks on Pi vrs Mac
if raspberry:
    theNetworks = ['eth0','wlan0']
else:
    theNetworks = ['en0']

# create the tables to hold the data
c.execute('''CREATE TABLE IF NOT EXISTS network_log
            (id INTEGER PRIMARY KEY,
            t TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            url TEXT,
            size INTEGER,
            status INTEGER,
            interface TEXT)''')

c.execute('''CREATE TABLE IF NOT EXISTS ping_log
            (id INTEGER PRIMARY KEY,
            t TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            ip TEXT,
            status INTEGER,
            interface TEXT)''')

c.execute('''CREATE TABLE IF NOT EXISTS dig_log
            (id INTEGER PRIMARY KEY,
            t TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            dnsused TEXT,
            host TEXT,
            resolvedip TEXT,
            expectedip TEXT,
            status INTEGER)''')

conn.commit()

# Ensure that there is a maximum of 8 URLS (otherwise the Sense Hat array addressing will fail)
theUrls =   [
            'http://192.168.0.1', #localhost
            'http://198.41.214.162', #cloudflare
            'http://216.58.200.100', #google
            'http://203.109.185.25', #vodafone
            'http://www.cloudflare.com',
            'http://www.google.com',
            'http://www.vodafone.co.nz'
            ]

theIPs =    [
            '192.168.0.1', #router
            '203.97.78.43', #vodafone DNS
            '8.8.8.8', #google DNS
            '198.41.214.162' #cloudflare
            ]

theDNSserver = [
            ['ns1.telstraclear.net','203.97.78.43'],
            ['google-public-dns-a.google.com','8.8.8.8']
            ]



theCurlCmd = 'curl --max-time 4 -s --write-out %{size_download} --output /dev/null'
theDigCmd = 'dig +time=4 +short '
if raspberry:
    thePingCmd = 'ping -c 1 -w 4'
else:
    # ping command on Mac slightly different
    thePingCmd = 'ping -c 1 -t 4'


overallstatus = 0

# ----------------------------------------
# loop through the URLS
for idx, interface in enumerate(theNetworks):
    # loop through the interfaces
    for idy, url in enumerate(theUrls):
        try:
            # query a unique .htm page to try and avoid caching (does not work on router, hence conditional)
            if url != 'http://192.168.0.1':
                datasize = subprocess.check_output(theCurlCmd + " --interface " + interface + " --url " + url + "/" + str(uuid.uuid1()) + ".htm", shell=True)
            else:
                datasize = subprocess.check_output(theCurlCmd + " --interface " + interface + " --url " + url, shell=True)
            status = 0
        except subprocess.CalledProcessError as e:
            datasize = 0
            status = e.returncode

        # store into the database
        print str(status) + " " + str(datasize) + " " + url
        c.execute("INSERT INTO network_log (url, size, status, interface) VALUES ('" +
            url + "'," +
            str(datasize) + "," +
            str(status) + "," +
            "'" + interface + "'" +
            ")")

        overallstatus += status

        if raspberry:
            #display on the sense hat
            if status > 0:
                sense.set_pixel(idx, idy, red)
            else:
                sense.set_pixel(idx, idy, green)

# ----------------------------------------
# loop through the IPs to attempt the ping
for idx, interface in enumerate(theNetworks):
    # loop through the interfaces
    for idy, ip in enumerate(theIPs):
        try:
            if raspberry:
                returndata = subprocess.check_output(thePingCmd + " -I " + interface + " " + ip, shell=True)
            else:
                returndata = subprocess.check_output(thePingCmd + " " + ip, shell=True)
            status = 0
        except subprocess.CalledProcessError as e:
            returndata = 0
            status = e.returncode

        # store into the database
        print str(status) + " " + ip
        c.execute("INSERT INTO ping_log (ip, status, interface) VALUES ('" +
            ip + "'," +
            str(status) + "," +
            "'" + interface + "'" +
            ")")

        overallstatus += status

        if raspberry:
            #display on the sense hat
            if status > 0:
                sense.set_pixel(idx+3, idy, red)
            else:
                sense.set_pixel(idx+3, idy, green)


# ----------------------------------------
# I dig using dig...
# Only thing is that it returns 0 irrespective of query result (even on timeout)
# so need to parse the return database
# also can't specify the interface to use
# NOTE: Not actually sure this is working. Noticed successful result when modem off... need to check.
y = 0
for nameserver in theDNSserver:
    ns = nameserver[1]
    for hostname in theDNSserver:
        hn = hostname[0]
        hip = hostname[1]
        returndata = subprocess.check_output(theDigCmd + " @" + ns + " " + hn, shell=True).strip()
        if returndata == hip:
            status = 0
        else:
            status = 1

        # store into the database
        print str(status) + " " + hn + " returned " + returndata + " using ns " + ns
        c.execute("INSERT INTO dig_log (dnsused, host, resolvedip, expectedip, status) VALUES (" +
            "'" + ns + "'," +
            "'" + hn + "'," +
            "'" + returndata + "'," +
            "'" + hip + "'," +
            str(status) +
            ")")

        overallstatus += status

        if raspberry:
            #display on the sense hat
            if status > 0:
                sense.set_pixel(7, y, red)
            else:
                sense.set_pixel(7, y, green)

        y += 1

# ----------------------------------------
# Don't wake me up at night with flashing lights
if datetime.datetime.now().hour > 20 or datetime.datetime.now().hour < 7:
    pass
else:
    if raspberry and overallstatus > 0:
        sense_flash.flash(10,0.3)

# Write to the database
conn.commit()
conn.close()

# Write the (static) summary webpage.

# Get count of errors in last 24hrs for specific interface

# Get total count of errors for specific interface
