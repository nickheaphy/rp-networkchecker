# Use Raspberry Pi to monitor network

This is a quick and dirty hack that uses Curl to attempt to download data from a list of websites. Curl is setup to timeout after 4 sec in the advent that the website can't be accessed.

In addition it performs a ping test as the problem that I am attempting to monitor may be isolated to HTTP rather than ICMP (I don't really know as it might be a DNS problem...)

Uses both network interfaces of the Pi just in case the fault is with the wifi on the router under test.

Thanks to Ewan for additional DNS checks using Dig. This check name resolution using both Vodafone nameserver and Google nameservers to resolve themselves.

## Setup

Need remove file on Raspberry Pi `/etc/ifplugd/action.d/action_wpa` (this is a symlink to `../../wpa_supplicant/action_wpa.sh`) then bring up wlan0 using `sudo ifup wlan0`

Set the permissions to 755 and then add a `/etc/crontab` entry to run every minute.

## Database

The script does create a SQLite database with the status. The database format is as follows:

| id      | t         | url  | size    | status  | interface |
| ------- | --------- | ---- | ------- | ------- | --------- |
| INTEGER | TIMESTAMP | TEXT | INTEGER | INTEGER | TEXT      |

Where:

* ID - the primary key of the table
* t - the timestamp when the row was written
* url - the URL that was loaded
* size - the number of bytes downloaded
* status - the return status of Curl (0 is good, everything else bad)
* interface - the interface used to load the url

For the Ping test the database is similar

| id      | t         | ip   | status  | interface |
| ------- | --------- | ---- | ------- | --------- |
| INTEGER | TIMESTAMP | TEXT | INTEGER | TEXT      |

And the Dig database is

| id      | t         | dnsused | host | resolvedip | expectedip | status  |
| ------- | --------- | ------- | ---- | ---------- | ---------- | ------- |
| INTEGER | TIMESTAMP | TEXT    | TEXT | TEXT       | TEXT       | INTEGER |

## Feedback

Uses the Raspberry Pi Sense Hat to provide LED feedback on the status of each of the URLs trying to be accessed.

## Reporting

Have started work on generating a report. It is a work in progress. Use the command `generate_output_logs.py YYYY-MM-DD` (where YYYY-MM-DD is the date you want a report of) and it will generate a HTML report into the `output_logs` folder for that date. Uses Google Charts to make a graph showing the outages (which turns out is not the best when trying to dump 1440 columns of data, one column for every minute in the day)

### Todo

Still need to visualise the ping and dig results into the reporting along with some sort of summary.
