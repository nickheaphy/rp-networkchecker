try:
    from sense_hat import SenseHat
    import time
    sense = SenseHat()
except:
    pass


def flash(number, pause):
    '''Generate a flashing X on the Sense Hat
    Takes an number of flashes and a pause time between flashes as arguments'''

    X = [0, 0, 0]  # Black
    O = [255, 0, 0]  # Red

    bigX = [
    X, O, O, O, O, O, O, X,
    O, X, O, O, O, O, X, O,
    O, O, X, O, O, X, O, O,
    O, O, O, X, X, O, O, O,
    O, O, O, X, X, O, O, O,
    O, O, X, O, O, X, O, O,
    O, X, O, O, O, O, X, O,
    X, O, O, O, O, O, O, X
    ]

    pixel_list = sense.get_pixels()

    for counter in range(1,number):
        sense.set_pixels(bigX)
        time.sleep(pause)
        sense.set_pixels(pixel_list)
        time.sleep(pause)
