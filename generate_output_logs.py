import sqlite3
import time
import sys

path_to_placeholder = "output_logs/placeholder.htm"
path_to_save = "output_logs/"

def generate_html_logs(database,thedate):
    '''Generates html output from the database for the given date
    date in format 'YYYY-MM-DD' '''
    # I can feel this will be an ugly cludge

    # Open the placeholder HTML file (the irony that I am using online charting
    # to display network problems is not lost on me)
    try:
        placeholder = open(path_to_placeholder, 'r')
        placeholdertext = placeholder.read()
    except:
        print "Can't open placeholder.htm"
        exit()

    # open the database
    try:
        conn = sqlite3.connect(database)
        c = conn.cursor()
    except:
        print "Can't open database"
        exit()

    # get the interface list
    theInterfaces = []
    interfaceQuery = "SELECT DISTINCT interface FROM network_log WHERE date(t,'localtime') = ?"
    for row in c.execute(interfaceQuery, (thedate,)):
        theInterfaces.append(row[0])

    # get a list of the urls
    theURLs = []
    curlUrlQuery = "SELECT DISTINCT url FROM network_log WHERE date(t,'localtime') = ?"
    for row in c.execute(curlUrlQuery, (thedate,)):
        theURLs.append(row[0])

    # query each interface seperate
    theQueries = []
    for interface in theInterfaces:
        # now we build an ugly SQL query because SQLite does not have a pivot function
        fuglyQuery = "SELECT strftime('%H:%M', t, 'localtime') as thetime, "
        for url in theURLs:
            fuglyQuery += "COUNT(DISTINCT case when url = '" + url + "' and status !=0 then 1 end) as '" + url + "',"
        #need to pop the last comma
        fuglyQuery = fuglyQuery[:-1]
        fuglyQuery += " FROM network_log WHERE date(t,'localtime') = ? "
        fuglyQuery += " AND interface = '" + interface + "'"
        fuglyQuery += " GROUP BY thetime"
        # print fuglyQuery
        theQueries.append(fuglyQuery)

    # now actually perform the queries and build the google chart data
    googleDataList = []
    for aQuery in theQueries:
        googleData = "var data = google.visualization.arrayToDataTable([['IP',"
        for url in theURLs:
            googleData += "'" + url + "',"
        googleData += " { role: 'annotation' } ], ["
        for row in c.execute(aQuery, (thedate,)):
            for column in row:
                if ":" in str(column):
                    googleData += "'" + str(column) + "',"
                else:
                    googleData += str(column) + ","
            googleData += "''],["
        googleData = googleData[:-2]
        googleData += "]);"
        googleDataList.append(googleData)

    # replace the date
    placeholdertext = placeholdertext.replace("REPLACEDATE", thedate)

    # insert the curl data
    placeholdertext = placeholdertext.replace("// INSERT-CURL-DATA-NW1-HERE", googleDataList[0])
    placeholdertext = placeholdertext.replace("// INSERT-CURL-DATA-NW2-HERE", googleDataList[1])

    # save the HTML with thedate into the output folder
    try:
        file = open(path_to_save + thedate + ".htm",'w')
        file.write(placeholdertext)
        file.close
    except:
        print "Could not write the output file"

def main():
    if len(sys.argv) !=2:
        print("Please enter the date in YYYY-MM-DD format when calling.")
        print("Eg. > %s 2017-07-24" % (sys.argv[0]))
    else:
        #should really validate user input...
        generate_html_logs('networklog.db',sys.argv[1])

if __name__ == "__main__":
    main()
